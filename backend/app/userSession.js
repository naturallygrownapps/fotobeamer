module.exports = function (req, res, next) {
    req.userId = req.get('FBeam-UserId') || 'anonymous';
    next();
};