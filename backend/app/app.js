const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');

const app = (function initApp() {
    function normalizePort(val) {
        var port = parseInt(val, 10);

        if (isNaN(port)) {
            // named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        return false;
    }

    var app = express();
    var port = normalizePort(process.env.PORT || '3010');
    app.set('port', port);
    var server = http.createServer(app);
    app.getServer = () => server;
    return app;
})();
const router = express.Router();
const io = require('socket.io')(app.getServer(), {
    path: '/api/socket.io'
});

const config = require('./config');

const userSession = require('./userSession');

const index = require('./routes/index');
const images = require('./routes/images');
const display = require('./routes/display')(io);

// view engine setup
app.set('views', path.join(__dirname, 'views'));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cors());
// Enable cors preflight on all routes.
app.options('*', cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(userSession);
if (config.getRequireAuth()) {
    app.use(require('./basicAuth'));
}

function nocache(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
}

router.use('/images', nocache, images);
router.use('/display', nocache, display);
router.use('/', express.static(path.join(__dirname, 'public')));
app.use('/api', router);
app.use(['/'], express.static(path.join(__dirname, 'public', 'frontend')));
app.use(['/display'], express.static(path.join(__dirname, 'public', 'frontend', 'index.html')));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        console.error(err.stack.split("\n"))
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

// production error handler
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: err
    });
});


module.exports = app;
