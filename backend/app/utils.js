const fs = require('fs-extra');

module.exports.ensurePathAsync = function(path) {
    return new Promise((resolve, reject) => {
        fs.access(path, fs.F_OK, (err) => {
            if (err) {
                fs.mkdirs(path, (err) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(path);
                    }
                })
            }
            else {
                resolve(path);
            }
        });
    });
};