const path = require('path');
const fs = require('fs-extra');
const _ = require("lodash");
const chokidar = require('chokidar');

class OrderBehavior {
    getNext() { throw new Error('This is abstract.'); }
    notifyAdd() { throw new Error('This is abstract.'); }
    notifyRemove(index) { throw new Error('This is abstract.'); }
}
module.exports.OrderBehavior = OrderBehavior;

class RandomLinearOrderBehavior extends OrderBehavior {
    constructor() {
        super();
        this.indices = [];
        this.currentIdx = -1;
    }

    notifyAdd() {
        this.indices.splice(0, 0, this.indices.length);
    }

    notifyRemove(index) {
        let localIdx = this.indices.indexOf(index);
        this.indices.splice(localIdx, 1);
    }

    getNext() {
        // We start with a linear array that we want shuffled.
        if (this.currentIdx === -1) {
            this.indices = _.shuffle(this.indices);
            this.currentIdx = 0;
        }

        this.currentIdx++;
        if (this.currentIdx >= this.indices.length) {
            this.indices = _.shuffle(this.indices);
            this.currentIdx = 0;
        }
        return this.indices[this.currentIdx];
    }
}
module.exports.RandomLinearOrderBehavior = RandomLinearOrderBehavior;

class NewestFifoOrderBehavior extends OrderBehavior {
    constructor() {
        super();
        this.nextItemsQueue = [];
    }

    notifyAdd(path) {
        this.nextItemsQueue.push(path);
    }

    notifyRemove(index, path) {
        // If the item is on the queue, remove it.
        let idx = this.nextItemsQueue.indexOf(path);
        if (idx >= 0) {
            this.nextItemsQueue.splice(idx, 1);
        }
    }

    reset() {
        this.nextItemsQueue.length = 0;
    }

    getNext() {
        // If we have items on the queue...
        if (this.nextItemsQueue.length) {
            // Remove an item from the queue and return it.
            let next = this.nextItemsQueue[0];
            this.nextItemsQueue.splice(0, 1);
            return next;
        }

        return null;
    }
}
module.exports.NewestFifoOrderBehavior = NewestFifoOrderBehavior;

class ImagePath {
    constructor(path) {
        this.notificationHandlers = [];
        this.path = path;
        this.files = [];
        this.isInitialized = false;
    }

    addNotificationHandler(notificationHandler) {
        this.notificationHandlers.push(notificationHandler);
    }

    onFileAdded(path) {
        if (this.files.indexOf(path) === -1) {
            this.files.push(path);
            _.forEach(this.notificationHandlers, n => n.notifyAdd(path));
        }
    }

    onFileRemoved(path) {
        let idx = this.files.indexOf(path);
        if (idx >= 0) {
            this.files.splice(idx, 1);
            _.forEach(this.notificationHandlers, n => n.notifyRemove(idx, path));
        }
    }

    initAsync() {
        if (this.isInitialized) {
            return Promise.resolve();
        }
        this.isInitialized = true;
        let self = this;
        return new Promise((resolve, reject) => {
            chokidar.watch(self.path, {
                    ignored: /\.thumb$/,
                    ignoreInitial: false,
                    awaitWriteFinish: {
                        stabilityThreshold: 2000,
                        pollInterval: 100
                    }
                })
                .on('add', _.bind(self.onFileAdded, self))
                .on('unlink', _.bind(self.onFileRemoved, self))
                .on('ready', resolve)
                .on('error', reject);
        }).catch(() => self.isInitialized = false);
    }
}
const getImagePath = (function() {
    let imagePaths = {};
    function getImagePath(path) {
        var imagePath = imagePaths[path];
        if (!imagePath) {
            imagePath = new ImagePath(path);
            imagePaths[path] = imagePath;
        }
        return imagePath;
    }

    getImagePath.getAll = () => imagePaths;

    return getImagePath;
})();

class QueueEntryWeight {
    constructor(priority) {
        this.priority = priority;
    }
}

class ImageQueueEntry {
    constructor(id, imagePath, priority, orderBehavior) {
        this.id = id;
        this.imagePath = imagePath;
        this.weight = new QueueEntryWeight(priority);
        this.orderBehavior = orderBehavior;
    }
}

module.exports.ImageQueue = class ImageQueue {

    constructor() {
        this.entries = [];
    }

    activateAsync() {
        return Promise.all(_.map(getImagePath.getAll(), p => p.initAsync()));
    }

    calculateEntryHitValues(entryList) {
        entryList = _.sortBy(entryList, e => e.weight.priority);
        let prioSum = _.reduce(entryList, function(result, value) {
            result += value.weight.priority;
            return result;
        }, 0);

        let weightValues = {};
        _.forEach(entryList, e => {
            weightValues[e.id] = e.weight.priority / prioSum;
        });

        let curHitValue = 0;
        let hitValues = {};
        _.forEach(entryList, e => {
            curHitValue += weightValues[e.id];
            hitValues[e.id] = curHitValue;
        });

        return hitValues;
    }

    sortEntriesByHitValues(entryList, hitValues) {
        entryList = _.sortBy(entryList, e => hitValues[e.id]);
        return entryList;
    }

    addImagesPath(path, priority, orderBehavior) {
        let p = getImagePath(path);
        p.addNotificationHandler(orderBehavior);
        this.entries.push(new ImageQueueEntry(_.uniqueId('imageQueueEntry_'), p, priority, orderBehavior));
        let hitValues = this.calculateEntryHitValues(this.entries);
        this.entries = this.sortEntriesByHitValues(this.entries, hitValues);
    }

    getWeightedRandomEntry(entryList, hitValues) {
        let rand = Math.random();
        let selectedEntry = null;

        for (let i = 0; i < entryList.length; i++) {
            let e = entryList[i];
            let hitValue = hitValues[e.id];
            if (rand < hitValue) {
                selectedEntry = e;
                break;
            }
        }
        return selectedEntry;
    }

    *iterateEntriesInWeightedRandomOrder () {
        // We are going to modify the collection, thus we need a copy.
        let entries = this.entries.slice();
        let hitValues = null;

        while(entries.length > 1) {
            let selectedEntry = this.getWeightedRandomEntry(entries, hitValues || this.calculateEntryHitValues(entries));
            yield selectedEntry;

            // Remove that entry and recalc weights.
            var idx = entries.indexOf(selectedEntry);
            entries.splice(idx, 1);
            hitValues = this.calculateEntryHitValues(entries);
            entries = this.sortEntriesByHitValues(entries, hitValues);
        }

        yield entries[0];
    }

    getNextImage() {
        for(let entry of this.iterateEntriesInWeightedRandomOrder()) {
            let nextImageEntry = entry.orderBehavior.getNext();
            if (nextImageEntry === 0 || nextImageEntry) {
                // Can be an index or an image path string.
                if (typeof nextImageEntry === 'number') {
                    return entry.imagePath.files[nextImageEntry];
                }
                else {
                    return nextImageEntry;
                }
            }
        }
    }
};
