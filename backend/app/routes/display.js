const express = require('express');
const path = require('path');
const fs = require('fs-extra');
const _ = require("lodash");
const EventEmitter = require('events');
const imageQueueModule = require('../imageQueue');
const ImageQueue = imageQueueModule.ImageQueue;
const config = require('../config');
const utils = require('../utils');
const gm = require('gm');

function reorientImageAsync(filepath) {
    return new Promise((resolve, reject) => {
        gm(filepath)
            .autoOrient()
            .write(filepath, err => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    resolve(filepath);
                }
            });
    });
}

class ImageSlideshow extends EventEmitter {
    constructor() {
        super();
        // The first queue works with fifo and stops when there are no new images. It is queried first. If this queue
        // cannot deliver an image, we use the fallback queue that randomly picks images from both user images and fallback images
        // where user images are priorized a bit.
        this.userImageQueue = new ImageQueue();
        this.fallbackImageQueue = new ImageQueue();
        this.currentImageUrl = '';
        // Set in init().
        this.updateInterval = null;
        this.isUpdatingCurrentImageUrl = false;
    }

    updateCurrentImageUrl() {
        const self = this;
        // gm works asynchronously and we don't want to get into trouble if it takes too long for
        // fast slideshows.
        if (self.isUpdatingCurrentImageUrl) {
            return;
        }

        self.isUpdatingCurrentImageUrl = true;

        try {
            // Queued user images are prioritized.
            let nextImage = self.userImageQueue.getNextImage();
            if (!nextImage) {
                // The fallback queue also contains user images, but not in fifo order, but randomly.
                nextImage = self.fallbackImageQueue.getNextImage();
            }

            if (nextImage) {
                // Copy the file to a temporary location so that it is "locked" while being used for the slideshow.
                const slideshowImagePath = path.join(config.getLocalSlideshowImagesPath(), 'current' + path.extname(nextImage));

                fs.copySync(nextImage, slideshowImagePath, {
                    clobber: true // overwrite existing file
                });

                reorientImageAsync(slideshowImagePath).then(
                    () => {
                        // Append a cache buster because we are always using the same filename.
                        self.currentImageUrl = ImageSlideshow.localImageToRemoteImage(slideshowImagePath) + '?t=' + (new Date()).getTime();
                        self.emit('imageUrlChanged', self.currentImageUrl);
                        self.isUpdatingCurrentImageUrl = false
                    },
                    e => {
                        console.error(e);
                        self.isUpdatingCurrentImageUrl = false
                    }
                );
            }
            else {
                self.isUpdatingCurrentImageUrl = false;
            }
        }
        catch (e) {
            self.isUpdatingCurrentImageUrl = false;
            throw e;
        }
    }

    getImageUrl() {
        return this.currentImageUrl;
    }

    startAsync() {
        var self = this;
        var newestFifoBehavior = new imageQueueModule.NewestFifoOrderBehavior();
        return Promise.all([
                utils.ensurePathAsync(config.getLocalSlideshowImagesPath()),
                utils.ensurePathAsync(config.getLocalFallbackImagesPath()).then(p => {
                    self.fallbackImageQueue.addImagesPath(p, 1, new imageQueueModule.RandomLinearOrderBehavior())
                }),
                utils.ensurePathAsync(config.getLocalUserImagesPath()).then(p => {
                    self.fallbackImageQueue.addImagesPath(p, 1, new imageQueueModule.RandomLinearOrderBehavior());
                    self.userImageQueue.addImagesPath(p, 1, newestFifoBehavior)
                })
            ])
            .then(() => Promise.all([
                self.fallbackImageQueue.activateAsync(),
                self.userImageQueue.activateAsync()
            ]))
            .then(() => {
                // We start clean, without any priority for user images.
                newestFifoBehavior.reset();
            })
            .then(() => {
                self.updateCurrentImageUrl();
                self.updateInterval = setInterval(() => {
                    self.updateCurrentImageUrl();
                }, config.getUpdateInterval());
            });
    }

    dispose() {
        if (this.updateInterval) {
            clearInterval(this.updateInterval);
            this.updateInterval = null;
        }
    }

    static localImageToRemoteImage(localImagePath) {
        let localBasePath = config.getLocalImagesPath();
        let remoteImagePath = path.relative(localBasePath, localImagePath);

        let remoteBasePath = config.getRemoteImagesPath();
        remoteImagePath = path.join(remoteBasePath, remoteImagePath).replace(/\\/g, '/');

        return remoteImagePath;
    }
}

function DisplayModule(io) {
    var router = express.Router();

    var imageSlideshow = new ImageSlideshow();
    let init = (function init() {
        return imageSlideshow.startAsync();
    })();
    imageSlideshow.on('imageUrlChanged', url => {
        io.emit('imageChanged', url);
    });

    router.get('/current', (req, res, next) => {
        init.then(() => {
            var imgUrl = imageSlideshow.getImageUrl();
            res.status(200).send(imgUrl);
        }, err => {
            next(err);
        });
    });
    
    return router;
}

module.exports = DisplayModule;
