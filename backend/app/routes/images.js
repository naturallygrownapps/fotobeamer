const express = require('express');
const path = require('path');
const fs = require('fs-extra');
const _ = require("lodash");
const Busboy = require('busboy');
const gm = require('gm');

const router = express.Router();
const config = require('../config');
const utils = require('../utils');

function getUserIdFromRequest(req) {
    if (!req || !req.userId) {
        return Promise.reject('Missing userid header.');
    }
    else {
        return req.userId;
    }
}

/**
 * Gets the publicly available local path to the directory for a user's images.
 */
function getLocalUserImagesPathAsync(req) {
    const userId = getUserIdFromRequest(req);
    let localImagesPath = config.getLocalUserImagesPath();
    localImagesPath = path.join(localImagesPath, userId);
    return utils.ensurePathAsync(localImagesPath);
}

/**
 * Gets a temporary directory per user into which images are copied when received for additional processing.
 */
function getlocalUserImagesTempPathAsync(req) {
    const userId = getUserIdFromRequest(req);
    let localImageTempPath = config.getLocalUserImagesTempPath();
    localImageTempPath = path.join(localImageTempPath, userId);
    return utils.ensurePathAsync(localImageTempPath);
}

/**
 * Gets the remote uri path to the user's images.
 */
function getRemoteUserImagesPath(req) {
    let basePath = config.getRemoteUserImagesPath();
    if (req && req.userId) {
        basePath = path.posix.join(basePath, req.userId);
    }
    return basePath;
}

function getLocalUserImageFiles(req) {
    return getLocalUserImagesPathAsync(req).then(imagesPath => {
        return new Promise((resolve, reject) => {
           fs.readdir(imagesPath, function (err, files) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(_.filter(files, f => path.extname(f) !== '.thumb'));
                }
            });
        });
    });
}

function localImageFileToUrl(baseUrl, imgFile) {    
    return path.posix.join(baseUrl, path.basename(imgFile));
}

router.get('/', function(req, res, next) {
    getLocalUserImageFiles(req).then(imgs => {
        var imagesBasePath = getRemoteUserImagesPath(req);
        imgs = _.map(imgs, _.partial(localImageFileToUrl, imagesBasePath));
        
        res.send(imgs);

    }, next);
});

function createThumbnailAsync(filepath) {
    return new Promise((resolve, reject) => {
        let thumbnailPath = filepath + '.thumb';
        gm(filepath)
            .resize(100)
            .write(thumbnailPath, err => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    resolve(thumbnailPath);
                }
            });
    });
}

function reorientImageAsync(filepath) {
    return new Promise((resolve, reject) => {
        gm(filepath)
            .autoOrient()
            .write(filepath, err => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    resolve(filepath);
                }
            });
    });
}

function copyFileAsyc(src, dst) {
    return new Promise((resolve, reject) => {
        fs.copy(src, dst, { replace: true }, function (err) {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}

function randomString() {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

router.post('/upload', function(req, res, next) {
    function imageProcessingErrorHandler(err) {
        console.error('Image processing/copying error.', err);
    }

    // Get our target directories first.
    Promise.all([
            getLocalUserImagesPathAsync(req),
            getlocalUserImagesTempPathAsync(req)
        ])
        .then(paths => {
            const localPublicImagesPath = paths[0];
            const localTempImagesPath = paths[1];
            const remoteImagesBasePath = getRemoteUserImagesPath(req);
            const busboy = new Busboy({ headers: req.headers });
            const receivedFiles = [];

            busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
                // Write file to temporary, user specific, directory.
                // This prevents issues with concurrent updates of the current slideshow image.
                // We don't want to show an incomplete/unprocessed image.
                let extension = path.extname(filename);
                let basename = path.basename(filename, extension);
                // Append a random string as iOS seems to send the same filename for all files.
                filename = basename + randomString() + extension;
                let targetFile = path.join(localTempImagesPath, filename);
                receivedFiles.push({
                    localfile: targetFile,
                    filename: filename
                });
                file.pipe(fs.createWriteStream(targetFile));
            });

            busboy.on('finish', function() {
                // All files received, now some postprocessing:
                Promise.resolve()   // Only for better code readability.
                    // Remove exif orientation.
                    .then(() => Promise.all(_.map(receivedFiles, receivedFile => reorientImageAsync(receivedFile.localfile).catch(imageProcessingErrorHandler))))
                    // Create thumbnails.
                    .then(() => Promise.all(_.map(receivedFiles, receivedFile => createThumbnailAsync(receivedFile.localfile).catch(imageProcessingErrorHandler))))
                    // Copy temporary files to public user directory.
                    .then((thumbnailPaths) => Promise.all([
                        Promise.all(_.map(receivedFiles, receivedFile =>
                            copyFileAsyc(receivedFile.localfile, path.join(localPublicImagesPath, receivedFile.filename)).catch(imageProcessingErrorHandler))),
                        Promise.all(_.map(thumbnailPaths, thumbnailPath =>
                            copyFileAsyc(thumbnailPath, path.join(localPublicImagesPath, path.basename(thumbnailPath))).catch(imageProcessingErrorHandler)))
                    ]))
                    .then(() => {
                        let newFileResources = _.map(receivedFiles, f => localImageFileToUrl(remoteImagesBasePath, f.filename));
                        res.status(201).json(newFileResources);
                    }, next);
            });

            req.pipe(busboy);
        }, next);
});

router.post('/delete', function(req, res, next) {
    var filepath = req.body.filepath;
    if (!filepath) {
        return next(new Error('Missing filename'));
    }
    
    var filename = path.basename(filepath);
    if (path.extname(filename) === '.thumb') {
        filename = filename.replace(/\.thumb$/,'');
    }

    getLocalUserImagesPathAsync(req).then(localUserImagesPath => {
        var userLocalFilePath = path.join(localUserImagesPath, filename);
        fs.access(userLocalFilePath, fs.F_OK, err => {
            if (err) {
                return next(err);
            }

            fs.unlink(userLocalFilePath, err => {
                if (err) {
                    return next(err);
                }
                fs.unlink(userLocalFilePath + '.thumb', err => {
                    if (err) {
                        console.error(new Error('Could not delete thumbnail for file ' + userLocalFilePath + '\n' + err));
                    }
                    res.status(204).end();
                });
            });
        });
    });
});

module.exports = router;