const path = require("path");

const publicPath = path.join(__dirname, 'public');

const config = {
    getRemoteUserImagesPath: () => 'images/userimages',
    getRemoteImagesPath: () => 'images',
    getLocalUserImagesPath: () => path.join(publicPath, 'images', 'userimages'),
    getLocalImagesPath: () => path.join(publicPath, 'images'),
    getLocalFallbackImagesPath: () => path.join(publicPath, 'images', 'other'),
    getLocalSlideshowImagesPath: () =>  path.join(publicPath, 'images', 'slideshow'),
    getLocalUserImagesTempPath: () => path.join(__dirname, 'tmp', 'userimages'),
    getUpdateInterval: () => 10000,
    getRequireAuth: () => false
};

module.exports = config;