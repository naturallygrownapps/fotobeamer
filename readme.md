# Fotobeamer Setup
This overview describes the setup as we used it on our wedding. The whole application was running in the local LAN only.

## Hardware
* Wifi-router with internet access so users don't lose their connectivity when connected to the local wedding network.
* Notebook in the LAN for the server software (see below).
* A domain that resolves to the IP of the local server so that guests don't have to type its IP address. Can be a real domain, but also one that exists only in the LAN, resolved by the DNS in the router.
* Another notebook with a camera on a tripod that has a remote shutter to allow guests to make photos of themselves ("photo booth"). Photos made here are uploaded automatically to the Fotobeamer server.

## Software
The frontend application is written in React and offers
* the photo-upload-app for guest's smartphones (served at `/`)
* the display-app for presenting the photo slideshow on a projector (`/display`)

The backend serves the frontend app at `/` and an API at `/api`. The API is used by the frontend to get, upload and delete user photos and to query the current slideshow photo. It also provides a websocket connection for slideshow update notifications.

The autouploader app watches a folder on disk for new files and uploads them automatically to the backend (behaving like a normal user to the `/api/upload` endpoint). This was used for the photo booth to get these photos displayed on the big screen automatically.

There are different queues to determine which image is going to be shown next:
* User uploads always have the highest priority and are shown next in FIFO order.
* If there are no images in the user uploads queue, images are presented in random order.
* Random images are picked from the user uploads and from images in `backend\app\public\images\other` (in the default configuration). You should always have some images in `other\` to have the slideshow running before people start uploading photos. Also this can nicely be used to just display some funny or old pictures.

There is no such thing as authentication for users. A random id is generated for each user upon opening the frontend app under which it identifies at the backend. Nothing stops other users from just identifying with the id of another user if they know it. Additional security is not necessarily needed, though, as everything just runs in a small local network that only wedding guests have access to.

## Quick Start

### Configuration
* Paths to image folder can be configured in `backend\app\config.js` (the defaults should work).
* autouploader must be configured with the correct folder to watch in `autouploader\config.js` (autouploader is optional).

### Run the app
1. Globally installed software prerequisites:
    * NodeJS >= 10
    * git
    * GraphicsMagick (for scaling imges) http://www.graphicsmagick.org/
1. Go to the `frontend\` folder and run `npm install`. Then execute `npm run deploy` to get the frontend built and copied to the backend's public folder.
1. Go to the `backend\` folder and run `npm install`. Then execute `npm start` to run the app. The server will run on port 3010 by default.

### Usage
* Open `localhost:3010` (or use the IP address/domain respectively) on the clients. Clients can now upload and manage their photos.
* Open `localhost:3010/display` (or use the IP address) on your projector machine. Browsers can be put into fullscreen by pressing F11.
* autouploader is started with `npm start` in the `autouploader` folder.

### Making Changes to the Code
The backend and autouploader files are just plain Node JS. The frontend is written in React.

For development, run `npm start` in each component's folder. The frontend will be reachable on `localhost:3000`, the backend on `localhost:3010`. The frontend will automatically connect to a local dev server in the dev environment. 

To update the frontend for production, run `npm run deploy` to build, bundle and copy the output to the backend's public folder.
