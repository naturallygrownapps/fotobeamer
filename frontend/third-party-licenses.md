* aargh font by Tup Wanders  
http://www.tupwanders.nl  
Licensed with a Creative Commons attribution license.
* Icons from Google's material design icons  
https://github.com/google/material-design-icons  
Apache License 2.0 https://github.com/google/material-design-icons/blob/master/LICENSE
