import React, { useState, useImperativeHandle, forwardRef } from "react";
import { CSSTransition } from "react-transition-group";

import "./ImageSlideshow.scss";

const ImageIds = {
    img1: "img1",
    img2: "img2"
};

function ImageSlideshow(props, ref) {
    // The urls of the images. Images are only animated in when fully loaded.
    const [imgSrc1, setImgSrc1] = useState("");
    const [imgSrc2, setImgSrc2] = useState("");
    // The currently shown image (type ImageIds).
    const [activeImage, setActiveImage] = useState("");
    // The image to animate in when it's loaded (type ImageIds).
    const [animationTargetImage, setAnimationTargetImage] = useState(null);

    // Expose updateImage function.
    useImperativeHandle(ref, () => ({
        updateImage
    }));

    function updateImage(newImage) {
        // Only animate to next image when we are not waiting for another one.
        if (!animationTargetImage) {
            if (activeImage === ImageIds.img1) {
                setAnimationTargetImage(ImageIds.img2);
                setImgSrc2(newImage);
            }
            else {
                setAnimationTargetImage(ImageIds.img1);
                setImgSrc1(newImage);
            }
        }
    }

    function imageLoaded() {
        // Image is fully loaded, we can now animate.
        if (animationTargetImage) {
            setActiveImage(animationTargetImage);
        }
        setAnimationTargetImage(null);
    }

    function imageError(e) {
        // Skip image change animation.
        setAnimationTargetImage(null);
    }

    return (
        <div className="image-slideshow">
            <CSSTransition in={activeImage === ImageIds.img1} timeout={1000} classNames="slideshow-image">
                <div className="slideshow-image">
                    <img src={imgSrc1}
                        onLoad={imageLoaded}
                        onError={imageError} />
                </div>
            </CSSTransition>
            <CSSTransition in={activeImage === ImageIds.img2} timeout={1000} classNames="slideshow-image">
                <div className="slideshow-image">
                    <img src={imgSrc2}
                        onLoad={imageLoaded}
                        onError={imageError} />
                </div>
            </CSSTransition>
        </div>
    );
}

export default forwardRef(ImageSlideshow);
