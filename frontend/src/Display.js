import React, { useRef, useEffect } from "react";
import ImageSlideshow from "./ImageSlideshow";

import "./Display.scss";

function Display(props) {

    const imageSlideshowRef = useRef();

    useEffect(() => {
        const socket = io(props.url, {
            path: "/api/socket.io"
        });
        socket.on("error", (e) => {
            console.error(e);
        });

        // Get the first image manually on startup.
        props.api.getCurrentImage().then(imgUrl => {
            return imageSlideshowRef.current.updateImage(imgUrl);
        })
            .finally(() => {
                socket.on("imageChanged", url => {
                    imageSlideshowRef.current.updateImage(props.api.apiUrl + url);
                });
            });

        return () => {
            socket.emit("end");
            socket.disconnect();
        };
    }, [props.api, props.url]);

    return (
        <div className="display-root">
            <ImageSlideshow ref={imageSlideshowRef} />
        </div>
    );
}

export default Display;
