import React, { useEffect, useState } from "react";

import "./PhotoComponent.scss";
import loadingGif from "./assets/images/spinner.gif";

export default function PhotoComponent(props) {
    const photo = props.photo;
    const [isLoading, setIsLoading] = useState(false);
    const [photoUrl, setPhotoUrl] = useState(null);

    useEffect(() => {
        if (photo.promise) {
            setIsLoading(true);
            photo.promise.then(url => {
                photo.src = url;
                setPhotoUrl(url);
                setIsLoading(false);
            });
        }
        else {
            setPhotoUrl(photo.src);
            setIsLoading(false);
        }
    }, [photo]);

    if (isLoading || !photoUrl) {
        return (
            <div className="loading-image">
                <img src={loadingGif} />
            </div>
        );
    }
    else {
        return (
            <div className="photo-container">
                <img src={photoUrl} />
                <div className="delete-photo">
                    <button onClick={() => props.onDelete(props.photo)} />
                </div>
            </div>
        );
    }
}
