let id = 0;

export default class Photo {
    constructor(srcOrCreationPromise) {
        this.id = id++;
        if (!!srcOrCreationPromise.then) {
            this.src = null;
            this.promise = srcOrCreationPromise;
        }
        else {
            this.src = srcOrCreationPromise;
            this.promise = null;
        }
    }
}