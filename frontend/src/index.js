import "react-app-polyfill/stable";
import "promise.prototype.finally";

import React from "react";
import ReactDOM from "react-dom";

import "./index.scss";
import App from "./App";
import Modal from "react-modal";

const rootElement = document.getElementById("root");
Modal.setAppElement(rootElement);
ReactDOM.render(<App />, rootElement);
