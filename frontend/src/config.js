const backendUrl = process.env.REACT_APP_BACKEND || window.location.origin;

const config = {
    backendUrl,
    apiUrl: `${backendUrl}/api/`,
    title: process.env.REACT_APP_TITLE
};

export default config;
