import React, { useState, useEffect } from "react";
import "./MainScreen.scss";
import PhotoComponent from "./PhotoComponent";
import Modal from "react-modal";

import cameraLogo from "./assets/images/camera.svg";
import { ReactComponent as Arrow } from "./assets/images/arrow.svg";
import loadingGif from "./assets/images/spinner.gif";

const modalStyles = {
    overlay: {
        backgroundColor: "rgba(0, 0, 0, 0.5)"
    },
    content : {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)"
    }
};

function HeaderComponent(props) {
    return (
        <div className="header">
            <img className="logo" src={cameraLogo} height="30px" alt="" onClick={props.onLogoClick} />
            <span className="title">{props.title}</span>
        </div>
    );
}

export default function MainScreen(props) {

    const [photos, setPhotos] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [photoToDelete, setPhotoToDelete] = useState(null);
    const [showFooter, setShowFooter] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        props.api.getMyPhotos()
            .then(photos => {
                setPhotos(photos);
            })
            .finally(() => {
                setIsLoading(false);
            });
    }, [props.api]);
    
    function onFileForUploadSelected(e) {
        const input = e.target;
        const files = input.files;
        setPhotos(photos.concat(props.api.uploadPhotos([...files])));
        input.value = "";
    }

    function cancelDeletePhoto() {
        setPhotoToDelete(null);
    }

    function performDeletePhoto() {
        const toDelete = photoToDelete;
        setPhotoToDelete(null);
        if (toDelete) {
            setIsLoading(true);
            setPhotos(photos.filter(p => p !== toDelete));
            props.api.deletePhoto(photoToDelete.src)
                .catch(function (err) {
                    // ignore "file does not exist" errors.
                    if (!err || !err.data || !err.data.error || err.data.error.code !== "ENOENT") {
                        alert(`Fehler - Foto konnte nicht gelöscht werden.\n${JSON.stringify(err)}`);
                        // Add the photo back.
                        setPhotos(photos);
                    }
                })
                .finally(() => setIsLoading(false));
        }
    }

    function onLogoClick() {
        setShowFooter(!showFooter);
    }

    return (
        <>
            <HeaderComponent title={props.title} onLogoClick={onLogoClick} />
            <div className="content">
                {(() => {
                    if (isLoading) { 
                        return (
                            <div className="global-spinner">
                                <div>Deine Fotos werden geladen...</div>
                                <img src={loadingGif} />
                            </div>
                        );
                    }
                    else {
                        return (
                            <>
                                <label htmlFor="image-selection-input" className="lbl-add-photo">
                                    Foto hinzufügen
                                </label>
                                <input id="image-selection-input" type="file" accept="image/*" onChange={onFileForUploadSelected}></input>

                                {(() => {
                                    if (photos && photos.length) {
                                        return (
                                            <div className="photos">
                                                {photos.map(p => <PhotoComponent key={p.id} photo={p} onDelete={p => setPhotoToDelete(p)} />)}
                                            </div>
                                        );
                                    }
                                    else {
                                        return (
                                            <div className="no-photos">
                                                <Arrow className="action-arrow" />
                                                <div className="action-text">Hier ist es leer! Zeig uns doch ein paar deiner Fotos!</div>
                                            </div>
                                        );
                                    }
                                    
                                })()}

                                <Modal
                                    isOpen={!!photoToDelete}
                                    onRequestClose={cancelDeletePhoto}
                                    contentLabel="Willst du dieses Foto wirklich löschen?"
                                    style={modalStyles}>
                                    <div className="modal-delete-photo">
                                        <div className="title">Willst du dieses Foto wirklich löschen?</div>
                                        <img src={photoToDelete ? photoToDelete.src : ""} />
                                        <div className="buttons">
                                            <button className="btn-yes primary" onClick={performDeletePhoto}>Ja</button>
                                            <button className="btn-no" onClick={cancelDeletePhoto}>Nein</button>
                                        </div>
                                    </div>
                                </Modal>
                            </>
                        );
                    }
                })()}
                
            </div>
            {showFooter && <div className="footer">Id: {props.api.session.getUserId()}</div>}
        </>
    );
}
