import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Display from "./Display";
import FbeamApi from "./FbeamApi";
import FbeamSession from "./FbeamSession";
import config from "./config";
import MainScreen from "./MainScreen";

const session = new FbeamSession();
const api = new FbeamApi(config.apiUrl, session);

function App() {
    return (
        <Router>
            <>
                <Route path="/" exact render={() => <MainScreen title={config.title} api={api} />} />
                <Route path="/display/" render={() => <Display api={api} url={config.backendUrl} />} />
            </>
        </Router>
    );
}

export default App;
