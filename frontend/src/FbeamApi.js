import Photo from "./Photo";

function exposeNetworkErrors(fetchPromise) {
    return fetchPromise.then(response => {
        if (response.ok) {
            return response;
        } else {
            return response.text().then(t => {
                let json;
                try {
                    json = JSON.parse(t);
                }
                catch(e) {
                    json = t;
                }
                return Promise.reject(json);
            });
        }
    });
}

export default class FbeamApi {
    constructor(apiUrl, session) {
        this.apiUrl = apiUrl;
        this.session = session;
    }

    /**
     * @return {RequestInit} 
     */
    getFetchConfig() {
        return {
            headers: {
                "FBeam-UserId": this.session.getUserId()
            },
        };
    }

    getMyPhotos() {
        return exposeNetworkErrors(fetch(`${this.apiUrl}images`, this.getFetchConfig()))
            .then(response => response.json())
            .then(photoUrls => photoUrls.map(p => new Photo(this.getThumbnailPath(p))));
    }

    deletePhoto(filepath) {
        const config = {
            ...this.getFetchConfig(),
            method: "POST",
            body: JSON.stringify({
                filepath
            })
        };
        config.headers = {
            ...config.headers,
            "Content-Type": "application/json"
        };

        return exposeNetworkErrors(fetch(`${this.apiUrl}images/delete`, config));
    }

    getCurrentImage() {
        return exposeNetworkErrors(fetch(`${this.apiUrl}display/current`, this.getFetchConfig()))
            .then(response => response.text())
            .then(imageUrl => this.apiUrl + imageUrl);
    }

    getThumbnailPath(imageUrl) {
        return `${this.apiUrl}${imageUrl}.thumb`;
    }

    uploadPhotos(files) {
        if (files && files.length) {
            return files.map(f => {
                const formData = new FormData();
                formData.append(f.name, f);
                return new Photo(exposeNetworkErrors(fetch(`${this.apiUrl}images/upload`, {
                    ...this.getFetchConfig(),
                    method: "POST",
                    body: formData
                }))
                    .then(response => response.status === 201 ? response.json() : null)
                    .then(uploadedImageFiles => {
                        if (uploadedImageFiles && uploadedImageFiles.length) {
                            return this.getThumbnailPath(uploadedImageFiles[0]);
                        }
                    })
                    .catch(() => null)
                );
            });
        }

        return [];
    }
}
