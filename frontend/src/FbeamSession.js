function makeid() {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

export default class FbeamSession {
    getUserId() {
        let userId = localStorage.fbeamUserId;
        if (!userId) {
            userId = makeid();
            localStorage.fbeamUserId = userId;
        }
        return userId;
    }
}
