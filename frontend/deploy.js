const path = require('path');
const rimraf = require('rimraf');
const fs = require('fs-extra')

const frontendDir = path.join(__dirname, '../backend/app/public/frontend');
rimraf(frontendDir, {
    disableGlob: true
}, (err) => {
    if (err) {
        return console.error(err);
    }

    fs.copy('./build/', frontendDir, (err) => {
        if (err) {
            return console.error(err);
        }

        console.log('Done.');
    })
});
