module.exports = {
    "extends": "react-app",
    "rules": {
        "indent": [
            "error",
            4
        ],
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "always"
        ],
        "jsx-a11y/alt-text": 0
    },
    "globals": {
        "io": "readonly"
    }
};