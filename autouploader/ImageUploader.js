const unirest = require('unirest');


class ImageUploader {
    constructor(apiUrl, userId) {
        this.apiUrl = apiUrl;
        this.userId = userId;
    }
    
    uploadImage(path) {
        const self = this;
        return new Promise((resolve, reject) =>
            unirest.post(self.apiUrl + 'images/upload')
                .header('Content-Type', 'multipart/form-data')
                .header('FBeam-UserId', self.userId)
                .attach('file', path)
                .end(function (response) {
                    if (response.ok) {
                        resolve(response.body);
                    }
                    else {
                        reject(response.error);
                    }
                })
        );
    }
}

module.exports = ImageUploader;