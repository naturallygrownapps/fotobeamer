const path = require('path');
const fs = require('fs-extra');
const log = require('debug')('Config');

const jsonConfig = fs.readJsonSync(path.join(__dirname, 'config.json'), {
    throws: false
}) || {};

const config = {
    getApiUrl: () => jsonConfig.apiUrl || 'http://localhost:3010/api/',
    getUploadDelay: () => jsonConfig.uploadDelay || 60 * 1000,
    getWatchedPath: () => jsonConfig.watchedPath || 'C:\\Users\\rufus\\Pictures\\autouploadertest',
    getUserId: () => jsonConfig.userId || 'FotoeckeAutoupload'
};

log('apiUrl: ' + config.getApiUrl());
log('uploadDelay: ' + config.getUploadDelay());
log('watchedPath: ' + config.getWatchedPath());
log('userId: ' + config.getUserId());

module.exports = config;
