#!/usr/bin/env node

process.env['DEBUG'] = '*';

var debug = require('debug')('App');
var config = require('./config');
var AutoImageUploader = require('./AutoImageUploader');

process.on('uncaughtException', function(err) {
    console.error(err)
});
process.on('unhandledRejection', function(reason, p){
    console.error(reason, p);
});

var autoImageUploader = new AutoImageUploader(config);
autoImageUploader.start().then(() => {
    debug('Watching folder ' + config.getWatchedPath() + '...');
}, e => debug('Error' + JSON.stringify(e)));