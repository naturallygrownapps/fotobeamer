const fs = require('fs');
const _ = require('lodash');
const chokidar = require('chokidar');
const errorLog = require('debug')('AutoImageUploader:error');
const log = require('debug')('AutoImageUploader:debug');
const Imageuploader = require('./ImageUploader');

function fileExists(filePath) {
    try {
        fs.accessSync(filePath, fs.R_OK);
    }
    catch(e) {
        return false;
    }

    return true;
}

class AutoImageUploader {

    constructor(config) {
        this.config = config;
        this.uploader = new Imageuploader(config.getApiUrl(), config.getUserId());
        this.pendingUploads = {};
    }

    clearPendingUpload(filePath) {
        var uploadTask = this.pendingUploads[filePath];
        if (uploadTask) {
            clearTimeout(uploadTask);
            delete this.pendingUploads[filePath];
        }
    }

    getUploadFunction(filePath) {
        var self = this;
        return () => {
            delete this.pendingUploads[filePath];
            if (fileExists(filePath)) {
                self.uploader.uploadImage(filePath).then(
                    () => log('Image uploaded: ' + filePath),
                    e => errorLog(JSON.stringify(e))
                );
            }
        };
    }

    onFileAdded(filePath) {
        this.clearPendingUpload(filePath);
        this.pendingUploads[filePath] = setTimeout(this.getUploadFunction(filePath), this.config.getUploadDelay());
    }

    onFileRemoved(filePath) {
        this.clearPendingUpload(filePath);
    }

    start() {
        const self = this;
        return new Promise((resolve, reject) => {
            chokidar.watch(self.config.getWatchedPath(), {
                ignored: /\.thumb$/,
                ignoreInitial: true,
                awaitWriteFinish: {
                    stabilityThreshold: 2000,
                    pollInterval: 100
                }
            })
                .on('add', _.bind(self.onFileAdded, self))
                .on('unlink', _.bind(self.onFileRemoved, self))
                .on('ready', resolve)
                .on('error', reject);
        });
    }
}

module.exports = AutoImageUploader;